<?php
/**
 * Login.php
 * 免登接口
 * @author Deepseath
 * @version $Id$
 */
namespace deepseath\xinrenxinshi\Api;

use deepseath\xinrenxinshi\Xinrenxinshi;

class Login
{
    /**
     * 基类服务对象
     * @var \deepseath\xinrenxinshi\Xinrenxinshi
     */
    protected $service = null;

    /** 跳转类型：PC 端 */
    const REDIRECT_TYPE_PC = 0;
    /** 跳转类型：H5 端 */
    const REDIRECT_TYPE_H5 = 1;

    /** 用户类型：员工免登 */
    const USER_TYPE_NORMAL = 0;
    /** 用户类型：管理员免登 */
    const USER_TYPE_ADMIN = 1;
    /** 用户类型：管理员优先免登（需要单独申请权限） */
    const USER_TYPE_FIRSTADMIN = 2;

    /** 跳转页面类型：首页 */
    const REDIRECT_URL_TYPE_HOMEPAGE = 0;
    /** 跳转页面类型：审批列表 */
    const REDIRECT_URL_TYPE_WORKFLOW = 1;
    /** 跳转页面类型：工资条列表 */
    const REDIRECT_URL_TYPE_PAYROLL = 2;
    /** 跳转页面类型：考勤列表 */
    const REDIRECT_URL_TYPE_ATTENDANCE = 3;
    /** 跳转页面类型：招聘列表 */
    const REDIRECT_URL_TYPE_RECRUITMENT = 4;
    /** 跳转页面类型：内容列表 */
    const REDIRECT_URL_TYPE_CMS = 5;
    /** 跳转页面类型：公告列表 */
    const REDIRECT_URL_TYPE_NOTICE = 6;
    /** 跳转页面类型：我的团队 */
    const REDIRECT_URL_TYPE_TEAM = 7;

    public function __construct(Xinrenxinshi $service)
    {
        $this->service = $service;
    }

    /**
     * 1.4.1 获取员工免登url
     * @desc 客户通过员工id免密登录到薪人薪事系统。目前支持登录到管理员或员工PC端以及员工的H5端系统。同时也支持免登跳转到指定功能页。
     * @see https://api.xinrenxinshi.com/doc/v3/page/free/login_v5.html
     * @param array $params
     * <pre>
     *  employeeId	String	Y	员工id
     *  redirectType	Integer	Y	跳转类型 0:PC端，1:H5端
     *  userType	Integer	N	默认0，用户类型，0:员工免登，1:管理员免登，2:管理员优先免登 (管理员免登需要单独申请权限)
     *  redirectUrlType	Integer	N	跳转页面类型，默认跳转首页,具体参数详见跳转类型类常量：REDIRECT_URL_TYPE_**
     *  redirectParam	Map<String,String>	N	免登跳转相关参数，具体参数详见跳转参数
     * </pre>
     * @return array
     */
    public function getUrl(array $params) : array
    {
        $params = array_merge([
            'employeeId' => '',
            'redirectType' => self::REDIRECT_TYPE_PC,
            'userType' => self::USER_TYPE_NORMAL,
            'redirectUrlType' => self::REDIRECT_URL_TYPE_HOMEPAGE,
            'redirectParam' => []
        ], $params);

        return $this->service->apiPost('/login/geturl', $params);
    }
}
