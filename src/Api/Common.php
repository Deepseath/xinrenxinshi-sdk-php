<?php
/**
 * Common.php
 * 通用接口
 * @author Deepseath
 * @version $Id$
 */
namespace deepseath\xinrenxinshi\Api;

use deepseath\xinrenxinshi\Xinrenxinshi;

class Common
{
    /**
     * 基类服务对象
     * @var \deepseath\xinrenxinshi\Xinrenxinshi
     */
    protected $service = null;

    public function __construct(Xinrenxinshi $service)
    {
        $this->service = $service;
    }

    /**
     * 1.3.1 获取城市列表
     * @desc 获取城市区域信息。
     * @see https://api.xinrenxinshi.com/doc/v3/page/area/area_v5.html
     * @return array
     */
    public function citys() : array
    {
        return $this->service->apiPost('/common/citys', []);
    }

    /**
     * 1.3.2 国籍列表
     * @desc 获取国籍列表信息，主要包含国籍名称和对应的ID，国籍名称用于页面展示，ID用于更新员工对应类型的字段信息
     * @see https://api.xinrenxinshi.com/doc/v3/page/area/country_v5.html
     * @return array
     */
    public function country() : array
    {
        return $this->service->apiPost('/common/country', []);
    }
}
