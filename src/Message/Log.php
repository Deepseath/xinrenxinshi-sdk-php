<?php
/**
 * Log.php
 * 消息订阅日志接口
 * @author Deepseath
 * @version $Id$
 */
namespace deepseath\xinrenxinshi\Message;

use deepseath\xinrenxinshi\Xinrenxinshi;

class Log
{
    /**
     * 基类服务对象
     * @var \deepseath\xinrenxinshi\Xinrenxinshi
     */
    protected $service = null;

    public function __construct(Xinrenxinshi $service)
    {
        $this->service = $service;
    }

    /**
     * 1.13.2 获取失败消息接口
     * @desc 获取系统推送失败的信息，只能查询最近7日的失败消息
     * @see https://api.xinrenxinshi.com/doc/v3/page/message/error_v5.html
     * @param array $params
     * @return array
     */
    public function faillist(array $params = [])
    {
        return $this->service->apiPost('/message/faillist', $params);
    }
}
