<?php
/**
 * Recive.php
 * 消息订阅处理
 * @author Deepseath
 * @version $Id$
 */
namespace deepseath\xinrenxinshi\Message;

use deepseath\xinrenxinshi\Xinrenxinshi;

class Receive
{
    /**
     * xinrenxinshi object
     * @var \deepseath\xinrenxinshi\Xinrenxinshi
     */
    private $_xinrenxinshi = null;

    /**
     * 消息类型：薪人薪事员工入职
     * @see https://api.xinrenxinshi.com/doc/v3/page/message/create.html
     * @var string
     */
    const TYPE_EMPLOYEE_ENTRY = 'employee_entry';

    /**
     * 消息类型：薪人薪事员工更新
     * @see https://api.xinrenxinshi.com/doc/v3/page/message/update.html
     * @var string
     */
    const TYPE_EMPLOYEE_UPDATE = 'employee_update';

    /**
     * 消息类型：薪人薪事员工删除
     * @see https://api.xinrenxinshi.com/doc/v3/page/message/delete.html
     * @var string
     */
    const TYPE_EMPLOYEE_DELETE = 'employee_delete';

    /**
     * 消息类型：薪人薪事员工离职
     * @see https://api.xinrenxinshi.com/doc/v3/page/message/leave.html
     * @var string
     */
    const TYPE_EMPLOYEE_DISMISS = 'employee_dismiss';

    /**
     * 消息类型：薪人薪事创建待入职员工
     * @see https://api.xinrenxinshi.com/doc/v3/page/message/employeePaddingStaff.html
     * @var string
     */
    const TYPE_EMPLOYEE_PENDING_ENTRY = 'employee_pending_entry';

    /**
     * 消息类型：薪人薪事员工分组字段添加
     * @see https://api.xinrenxinshi.com/doc/v3/page/message/employeeGroupAdd.html
     * @var string
     */
    const TYPE_EMPLOYEE_GROUP_ADD = 'employee_group_add';

    /**
     * 消息类型：薪人薪事员工分组字段更新
     * @see https://api.xinrenxinshi.com/doc/v3/page/message/employeeGroupUpdate.html
     * @var string
     */
    const TYPE_EMPLOYEE_GROUP_UPDATE = 'employee_group_update';

    /**
     * 消息类型：薪人薪事员工分组字段删除
     * @see https://api.xinrenxinshi.com/doc/v3/page/message/employeeGroupDelete.html
     * @var string
     */
    const TYPE_EMPLOYEE_GROUP_DELETE = 'employee_group_delete';

    /**
     * 消息类型：薪人薪事部门创建
     * @see https://api.xinrenxinshi.com/doc/v3/page/message/departmentCreate.html
     * @var string
     */
    const TYPE_DEPARTMENT_CREATE = 'department_create';

    /**
     * 消息类型：薪人薪事部门更新
     * @see https://api.xinrenxinshi.com/doc/v3/page/message/departmentUpdate.html
     * @var string
     */
    const TYPE_DEPARTMENT_UPDATE = 'department_update';

    /**
     * 消息类型：薪人薪事部门删除
     * @see https://api.xinrenxinshi.com/doc/v3/page/message/departmentDelete.html
     * @var string
     */
    const TYPE_DEPARTMENT_DELETE = 'department_delete';

    /**
     * 消息类型：薪人薪事部门启用
     * @see https://api.xinrenxinshi.com/doc/v3/page/message/departmentEnable.html
     * @var string
     */
    const TYPE_DEPARTMENT_ENABLE = 'department_enable';

    /**
     * 消息类型：薪人薪事部门停用
     * @see https://api.xinrenxinshi.com/doc/v3/page/message/departmentDisable.html
     * @var string
     */
    const TYPE_DEPARTMENT_DISABLE = 'department_disable';

    /**
     * 消息类型：薪人薪事岗位创建
     * @see https://api.xinrenxinshi.com/doc/v3/page/message/jobCreate.html
     * @var string
     */
    const TYPE_JOB_CREATE = 'job_create';

    /**
     * 消息类型：薪人薪事岗位编辑
     * @see https://api.xinrenxinshi.com/doc/v3/page/message/jobUpdate.html
     * @var string
     */
    const TYPE_JOB_UPDATE = 'job_update';

    /**
     * 消息类型：薪人薪事岗位删除
     * @see https://api.xinrenxinshi.com/doc/v3/page/message/jobDelete.html
     * @var string
     */
    const TYPE_JOB_DELETE = 'job_delete';

    /**
     * 消息类型：薪人薪事审批处于异常状态时
     * @see https://api.xinrenxinshi.com/doc/v3/page/message/flowTask.html
     * @var string
     */
    const TYPE_FLOW_TASK = 'flow_task';

    /**
     * 消息类型：薪人薪事审批处于待办状态时
     * @see https://api.xinrenxinshi.com/doc/v3/page/message/flowStep.html
     * @var string
     */
    const TYPE_FLOW_STEP = 'flow_step';

    /**
     * 消息类型：薪人薪事审批处于终结状态时
     * @see https://api.xinrenxinshi.com/doc/v3/page/message/flowProcess.html
     * @var string
     */
    const TYPE_FLOW_PROCESS = 'flow_process';

    /**
     * 消息类型：薪人薪事中的离职交接，通过此消息推送
     * @see https://api.xinrenxinshi.com/doc/v3/page/message/dismissHandover.html
     * @var string
     */
    const TYPE_DISMISS_HANDOVER = 'dismiss_handover';

    /**
     * 消息类型：薪人薪事中的消息通知，通过此消息推送
     * @see https://api.xinrenxinshi.com/doc/v3/page/message/notify.html
     * @var string
     */
    const TYPE_NOTIFY = 'notify';

    /**
     * 消息类型：薪人薪事成本中心创建
     * @see https://api.xinrenxinshi.com/doc/v3/page/message/costCenterCreate.html
     * @var string
     */
    const TYPE_COST_CENTER_CREATE = 'cost_center_create';

    /**
     * 消息类型：薪人薪事成本中心更新
     * @see https://api.xinrenxinshi.com/doc/v3/page/message/costCenterUpdate.html
     * @var string
     */
    const TYPE_COST_CENTER_UPDATE = 'cost_center_update';

    /**
     * 消息类型：薪人薪事成本中心删除
     * @see https://api.xinrenxinshi.com/doc/v3/page/message/costCenterDelete.html
     * @var string
     */
    const TYPE_COST_CENTER_DELETE = 'cost_center_delete';

    /**
     * 消息类型：当新安排招聘面试时
     * @see https://api.xinrenxinshi.com/doc/v3/page/message/recruimentInterviewAdd.html
     * @var string
     */
    const TYPE_RECRUIMENT_INTERVIEW_ADD = 'recruiment_interview_add';

    /**
     * 消息类型：当修改招聘面试时
     * @see https://api.xinrenxinshi.com/doc/v3/page/message/recruimentInterviewUpdate.html
     * @var string
     */
    const TYPE_RECRUIMENT_INTERVIEW_UPDATE = 'recruiment_interview_update';

    /**
     * 消息类型：当招聘面试取消时
     * @see https://api.xinrenxinshi.com/doc/v3/page/message/recruimentInterviewCancel.html
     * @var string
     */
    const TYPE_RECRUIMENT_INTERVIEW_CANCEL = 'recruiment_interview_cancel';

    public function __construct(Xinrenxinshi $xinrenxinshi)
    {
        $this->_xinrenxinshi = $xinrenxinshi;
    }

    /**
     * 获取请求的数据
     * @see https://api.xinrenxinshi.com/doc/v3/page/message/pushProcessConfig_v5.html
     * @return boolean|mixed
     */
    public function data()
    {
        $data = file_get_contents('php://input');
        $sign = isset($_GET['sign']) ? $_GET['sign'] : '';
        if ($sign != $this->_xinrenxinshi->sign($data)) {
            return false;
        }

        return json_decode(trim($data), true);
    }
}
