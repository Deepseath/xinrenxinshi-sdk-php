<?php
/**
 * config.php
 * 薪人薪事接口对接配置文件
 * @author Deepseath
 * @version $Id$
 */
use think\facade\Env;

return [
    // 薪人薪事分配的 appKey
    'appKey' => Env::get('xinrenxinshi.appKey', ''),
    // 薪人薪事分配的 appSecret
    'appSecret' => Env::get('xinrenxinshi.appSecret', ''),
    // 薪人薪事接口 URL 前缀，默认：https://api.xinrenxinshi.com
    'baseUrl' => Env::get('xinrenxinshi.baseUrl', 'https://api.xinrenxinshi.com'),
    // 薪人薪事接口版本号，默认：v5
    'version' => Env::get('xinrenxinshi.version', 'v5'),
    // 调试模式
    'debug' => Env::get('xinrenxinshi.debug', true)
];
